class Calculator:
    def add(self, a, b):
        return a+b

    def subtract(self, a, b):
        return a-b

    def multiply(self, a, b):
        return a*b

    def divide(self, a, b):
        if b == 0:
            print("b cannot be 0")
        else:
            return a/b


c = Calculator()
print(c.add(1,2))
