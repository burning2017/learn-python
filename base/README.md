# Python 基础

> <https://mofanpy.com/tutorials/python-basic/interactive-python/introduction/>

## 文件操作

```py
with open("new_file2.txt", "w") as f:
    f.writelines(["some text for file2...\n", "2nd line\n"])
```

### 读写模式

| mode | 意思                               |
| :--: | :--------------------------------- |
|  w   | （创建）写文本                     |
|  r   | 读文本，文件不存在会报错           |
|  a   | 在文本最后添加                     |
|  wb  | 写二进制 binary                    |
|  rb  | 读二进制 binary                    |
|  ab  | 添加二进制                         |
|  w+  | 又可以读又可以（创建）写           |
|  r+  | 又可以读又可以写, 文件不存在会报错 |
|  a+  | 可读写，在文本最后添加             |
|  x   | 创建                               |

## OS

### 文件目录操作

- os.getcwd() 当前目录
- os.listdir()
- os.makedirs()
- os.path.exists()

### 文件管理系统

- os.removedirs()
- shutil.rmtree()
- os.rename()

### 文件目录多种检验

- os.path.isfile()
- os.path.exists()
- os.path.isdir()
- os.path.basename()
- os.path.dirname()
- os.path.split()

## 正则表达式

### 不用正则的判断

- re.compile()
- ptn.search()

### 正则给额外信息

- re.search()

### 中文

- string.encode()

### 查找替换等更多功能

- re.search()
- re.match()
- re.findall()
- re.finditer()
- re.split()
- re.sub()
- re.subn()

## pickle/json 序列化

### Pickle

- pickle.dumps()
- pickle.dump()
- pickle.load()

### Json

- json.dumps()
- json.dump()
- json.load()

### Pickle 和 Json 的不同

|     对比     | Pickle                       | Json                                        |
| :----------: | :--------------------------- | :------------------------------------------ |
|   存储格式   | Python 特定的 Bytes 格式     | 通用 JSON text 格式，可用于常用的网络通讯中 |
|   数据种类   | 类，功能，字典，列表，元组等 | 基本和 Pickle 一样，但不能存类，功能        |
| 保存后可读性 | 不能直接阅读                 | 能直接阅读                                  |
|   跨语言性   | 只能用在 Python              | 可以跨多语言读写                            |
|   处理时间   | 长（需编码数据）             | 短（不需编码）                              |
|    安全性    | 不安全（除非你信任数据源）   | 相对安全                                    |

## 单元测试(Unittest)

### assert

|         assert          |                       含义                       |
| :---------------------: | :----------------------------------------------: |
|    assertEqual(a, b)    |                      a == b                      |
|  assertNotEqual(a, b)   |                      a != b                      |
|  assertTrue(condition)  |              condition 是不是 True               |
| assertFalse(condition)  |              condition 是不是 False              |
|   assertGreater(a, b)   |                      a > b                       |
| assertGreaterThan(a, b) |                      a >= b                      |
|    assertLess(a, b)     |                      a < b                       |
|  assertLessEqual(a, b)  |                      a <= b                      |
|     assertIs(a, b)      |          a is b，a 和 b 是不是同一对象           |
|    assertIsNot(a, b)    |        a is not b，a 和 b 是不是不同对象         |
|     assertIsNone(a)     |             a is None，a 是不是 None             |
|   assertIsNotNone(a)    |           a is not None，a 不是 None？           |
|     assertIn(a, b)      |              a in b, a 在 b 里面？               |
|    assertNotIn(a, b)    |            a not in b，a 不在 b 里？             |
|    assertRaises(err)    | 通常和 with 一起用，判断 with 里的功能是否会报错 |

## 字符串

### %百分号模式

- "%s" % string

| 方式 | 意思 |
| :--: | :--: |
|  %d  | 整数 |
|  %i  | 整数 |
|  %f  | 小数 |
|  %s  | 字符 |

```python
print("%f" % (1/3))     # 后面不限制
print("%.2f" % (1/3))   # 后面限制 2 个位置
print("%4d" % (1/3))    # 前面补全最大 4 个位置
print("%5d" % 12)       # 前面补全最大 5 个位置
```

### format 功能

- "{}".format()

| 方式 |                意思                |           code           |
| :--: | :--------------------------------: | :----------------------: |
|  :,  | 每 3 个 0 就用逗号隔开，比如 1,000 |  `'{:,}'.format(9999)`   |
|  :b  |           该数字的二进制           |  `'{:b}'.format(9999)`   |
|  :d  |               整数型               | `'{:3d}'.format(1.234)`  |
|  :f  |              小数模式              | `'{:.2f}'.format(1.234)` |
|  :%  |             百分比模式             |  `'{:.2%}'.format(100)`  |

### f 格式化字符串

- f"{n}"

```python
score
print(f"You scored {score:.2%}")
```

### 修改字符串

|    方式    |         意思         |
| :--------: | :------------------: |
|   strip    |   去除两端的空白符   |
|  replace   |       替换字符       |
|   lower    |    全部做小写处理    |
|   upper    |    全部做大写处理    |
|   title    |   仅开头的字母大写   |
|   split    |      按要求分割      |
|    join    |      按要求合并      |
| startswith | 判断是否为某字段开头 |
|  endswith  | 判断是否为某字段结尾 |

## python 小技巧

### Lambda

```py
add = lambda a, b: a+b
print(add(1,2))
```

### for 的简写模式

- 创建列表 `l = [i*2 for i in range(10)]`
- 创建字典 `d = {"i"+str(i): i*2 for i in range(10)}`

### if else 简写模式

```py
a = 1 if True else 2
```

### 一行 for + if

```py
[i*2 for i in range(10) if i%2==0]
```

### enumerate 自动加 index

```py
for i, item in enumerate(items)
```

### Zip 同时迭代

```py
for n, s in zip(items, scores)
```

### 反转列表

- `list.reverse()`
- `for i in reversed(list)`
- `_l = l[::-1]`

## Python 环境

- 直接安装 python
- [miniconda](https://docs.conda.io/en/latest/miniconda.html)/[anaconda](https://www.anaconda.com/products/individual#Downloads)

### pip 源

```sh
pip install -i https://mirrors.cloud.tencent.com/pypi/simple numpy
```

|  源名  |                    源地址                     |
| :----: | :-------------------------------------------: |
| 腾讯源 | <https://mirrors.cloud.tencent.com/pypi/simple> |
| 阿里源 |    <http://mirrors.aliyun.com/pypi/simple/>     |
| 豆瓣源 |        <http://pypi.douban.com/simple/>         |
| 清华源 |   <https://pypi.tuna.tsinghua.edu.cn/simple/>   |

#### 配置

- Mac

```sh
mkdir ~/.pip
cat << EOF > ~/.pip/pip.conf
[global]
index-url = https://mirrors.cloud.tencent.com/pypi/simple
trusted-host = mirrors.cloud.tencent.com
EOF
```

- Windows(C:\User\username\pip\pip.ini)

```txt
[global]
index-url = https://mirrors.cloud.tencent.com/pypi/simple
trusted-host = mirrors.cloud.tencent.com
```

### 迁移依赖

```sh
pip freeze > requirements.txt
```

```sh
pip install -r requirements.txt
```
